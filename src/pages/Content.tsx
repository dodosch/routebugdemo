import React, {useState} from 'react';
import {
    IonBackButton,
    IonButtons,
    IonContent,
    IonHeader, IonIcon, IonItem, IonLabel, IonList,
    IonPage, IonSpinner,
    IonToolbar,
    useIonViewWillEnter
} from '@ionic/react';
import {RouteComponentProps} from "react-router";
import {arrowForwardSharp} from "ionicons/icons";

interface ContentPageProps extends RouteComponentProps<{
    pid: string;
    isRoot: string;
}> {
}

const Content: React.FC<ContentPageProps> = ({match,history,location}) => {

    const [loading, setLoading] = useState(true);
    const [contents, setContents] = useState<string[]>([]);

    useIonViewWillEnter(() => {
        setLoading(true);
        //console.log(match);console.log(history);console.log(location);

        // do an API call or something ... then
        setLoading(false);
        setContents(["Content "+match.params.pid]);

    }, [match.params.pid])

    return (
        <IonPage>
            <IonHeader>
                {!match.params.isRoot && (
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonBackButton/>
                        </IonButtons>
                    </IonToolbar>
                )}
            </IonHeader>
            <IonContent>
                <IonList>
                    {loading ? (
                        <div className="ion-text-center">
                            <IonSpinner name={"lines"}/>
                        </div>
                    ) : (
                        <div>
                            {contents.map((content, index) => (
                                <IonItem key={index}>
                                    <h2>{content}</h2>
                                </IonItem>
                            ))}
                        </div>
                    )}
                    {match.params.isRoot && (
                        <div>
                            <IonItem routerLink={`/content/1`} routerDirection="forward" detail={false}>
                                <IonLabel><h2>Link 1</h2></IonLabel>
                                <IonIcon size="small" icon={arrowForwardSharp}/>
                            </IonItem>
                            <IonItem routerLink={`/content/2`} routerDirection="forward" detail={false}>
                                <IonLabel><h2>Link 2</h2></IonLabel>
                                <IonIcon size="small" icon={arrowForwardSharp}/>
                            </IonItem>
                        </div>
                    )}
                </IonList>
            </IonContent>
        </IonPage>
    );
};

export default Content;

